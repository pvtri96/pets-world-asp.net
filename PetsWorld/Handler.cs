﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace PetsWorld
{
    public class Handler
    {
        public Handler() { }

        public static string Connection()
        {
            return WebConfigurationManager.ConnectionStrings["PetsWorldDBConnection"].ConnectionString;
        }

        public static DataTable SelectModels(string sql)
        {
            string connect = Connection();
            SqlConnection con = new SqlConnection(connect);
            SqlDataAdapter da = new SqlDataAdapter(sql, con);
            DataTable dt = new DataTable();
            da.Fill(dt);

            return dt;
        }
    }
}
