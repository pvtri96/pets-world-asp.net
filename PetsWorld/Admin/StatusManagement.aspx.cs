﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace PetsWorld
{
    public partial class StatusManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getAllPetStatus();   
        }

        private void getAllPetStatus()
        {
            string query = string.Format(@"SELECT * FROM pet_status");
            
            DataTable dt = Handler.SelectModels(query);
            ListView_PetStatus.DataSource = dt;
            ListView_PetStatus.DataBind();
        }
    }
}
