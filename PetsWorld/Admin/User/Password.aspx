﻿<%@ Page Language="C#" MasterPageFile="~/Admin/User/User.master" AutoEventWireup="true" CodeBehind="Password.aspx.cs" Inherits="PetsWorld.Components.User.Profile" Title="User - Change Password" %>
<asp:Content ID="Content1" ContentPlaceHolderID="profile" runat="server">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 d-flex flex-column mx-auto no-spacing">
        <div class="form-group">
            <asp:Label AssociatedControlID="oldpassword" runat="server" Text="Current Password"/>
            <asp:TextBox TextMode="password" CssClass="form-control" ID="oldpassword" runat="server"/>
        </div>
        <div class="form-group">
            <asp:Label AssociatedControlID="newpassword" runat="server" Text="New Password"/>
            <asp:TextBox TextMode="Password" CssClass="form-control" ID="newpassword" runat="server"/>
        </div>
        <div class="form-group">
            <asp:Label AssociatedControlID="repassword" runat="server" Text="Re-enter Password"/>
            <asp:TextBox TextMode="Password" CssClass="form-control" ID="repassword" runat="server"/>
        </div>
        <div class="group-button ml-auto pt-4">
            <asp:Button CssClass="btn btn-primary" Text="Submit" runat="server" />
            <asp:Button CssClass="btn btn-danger" Text="Cancel" runat="server" />
        </div>
    </div>

</asp:Content>
