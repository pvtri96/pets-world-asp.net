﻿<%@ Page Language="C#" MasterPageFile="~/Admin/User/User.master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="PetsWorld.Components.User.Profile1" Title="User - Update Profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="profile" runat="server">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 no-spacing d-flex flex-column mx-auto">
        <div class="form-group">
            <asp:Label AssociatedControlID="fname" runat="server" Text="First name"/>
            <asp:TextBox CssClass="form-control" ID="fname" runat="server"/>
        </div>
        <div class="form-group">
            <asp:Label AssociatedControlID="lname" runat="server" Text="Last name"/>
            <asp:TextBox CssClass="form-control" ID="lname" runat="server" />
        </div>
        <div class="form-group">
            <asp:Label AssociatedControlID="mobile" runat="server" Text="Mobile Number"/>
            <asp:TextBox CssClass="form-control" ID="mobile" runat="server" />
        </div>
        <div class="form-group">
            <asp:Label AssociatedControlID="interest" runat="server" Text="Interests"/>
            <asp:TextBox CssClass="form-control" ID="interest" runat="server" />
        </div>
        <div class="form-group">
            <asp:Label AssociatedControlID="job" runat="server" Text="Occupation"/>
            <asp:TextBox CssClass="form-control" ID="job" runat="server" />
        </div>
        <div class="form-group">
            <asp:Label AssociatedControlID="facebook" runat="server" Text="Facebook"/>
            <asp:TextBox CssClass="form-control" ID="facebook" runat="server" />
        </div>
        <div class="form-group">
            <asp:Label AssociatedControlID="twitter" runat="server" Text="Twitter"/>
            <asp:TextBox CssClass="form-control" ID="twitter" runat="server" />
        </div>
        <div class="form-group">
            <asp:Label AssociatedControlID="google" runat="server" Text="Google"/>
            <asp:TextBox CssClass="form-control" ID="google" runat="server" />
        </div>
        <div class="form-group">
            <asp:Label AssociatedControlID="desc" runat="server" Text="Description"/>
            <asp:TextBox CssClass="form-control" ID="desc" runat="server" />
        </div>
        <div class="group-button ml-auto pt-4">
            <asp:Button CssClass="btn btn-primary" Text="Submit" runat="server"/>
            <asp:Button CssClass="btn btn-danger" Text="Cancel" runat="server"/>
        </div>
    </div>
</asp:Content>
