﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace PetsWorld
{
    public partial class TypeManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getAllPetTypes();
        }

        private void getAllPetTypes()
        {
            string query = string.Format(@"SELECT * FROM pet_types");

            DataTable dt = Handler.SelectModels(query);
            ListView_PetTypes.DataSource = dt;
            ListView_PetTypes.DataBind();
        }

        //protected void CreateType(object sender, EventArgs e)
        //{
        //    con.Open();
        //    cmd = new SqlCommand();

        //    string query = "INSERT INTO pet_types VALUES (@Name, @Description, 4)";

        //    SqlParameter param = new SqlParameter();

        //    cmd.Parameters.AddWithValue("@Name", Name.Text);
        //    cmd.Parameters.AddWithValue("@Description", Description.Text);

        //    cmd.CommandText = query;
        //    cmd.CommandType = CommandType.Text;
        //    cmd.Connection = con;
        //    cmd.ExecuteNonQuery();

        //    con.Close();
        //}
    }
}