﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="TypeManagement.aspx.cs" Inherits="PetsWorld.TypeManagement" Title="Type Management" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="TypeManagement" ContentPlaceHolderID="content" runat="server">
    <div class="model" id="main">
        <div class="row">
            <div class="create-area col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5">
                <div class="row">
                    <div class="create-area-content col-9 col-sm-11 col-md-10 col-lg-8 col-xl-8
                                justify-content-center align-items-center
                                mx-auto my-auto">
                        <h5 class="title">Type Management</h5>
                        <p class="layout-description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur commodi eum quae, ipsum modi odio nulla, aliquam blanditiis dolorem.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur commodi eum quae, ipsum modi odio nulla, aliquam blanditiis dolorem.
                        </p>
                        <div class="form-group">
                            <asp:Label ID="NameLable" CssClass="form-control-label" AssociatedControlID="name" runat="server" Text="Name"></asp:Label>
                            <asp:TextBox ID="Name" Name="Name" CssClass="form-control form-control-success form-control-danger" runat="server"/>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="DescriptionName" cssclass="form-control-label" AssociatedControlID="description" Text="Description" runat="server"></asp:Label>
                            <asp:TextBox ID="Description" Name="Description" CssClass="form-control form-control-success form-control-danger" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group features">
                            <asp:Button ID="Cancel" CssClass="btn btn-secondary" Text="Cancel" runat="server"/>
                            <asp:Button ID="Create" CssClass="btn btn-primary ml-2" Text="Create" 
                                runat="server"/>
                        </div>
                        <div class="notification">
                            <b><em><span class="message"></span>&nbsp;</em></b>
                        </div>
                    </div>
                </div>
            </div>
            <div class="list-area col-12 col-sm-7 col-md-7 col-lg-7 col-xl-7">
                <div class="row">
                    <div class="list-area-content col-9 col-sm-10 col-md-10 col-lg-8 col-xl-8
                                justify-content-center align-items-center
                                mx-auto my-auto">
                        <div class="form-group filter-area d-flex">
                            <asp:TextBox CssClass="form-control search-input" runat="server"/>
                            <asp:DropDownList CssClass="form-control option-input ml-1" runat="server">
                                <asp:ListItem Value="" Text="Select one"/>
                            </asp:DropDownList>
                            <asp:Button CssClass="btn btn-secondary btn-sm ml-1 reset-filter" runat="server" Text="Search"/>
                        </div>
                        <div class="list">
                            <div class="list-group">
                                <div class="list-group-item media">
                                    <div class="media-body">
                                        <div class="row">
                                            <div class="info col-12 text-truncate">
                                                <asp:ListView runat="server" DataKeyNames="id" ID="ListView_PetTypes">
                                                    <ItemTemplate>
                                                        <div class="col-12 d-flex py-2">
                                                            <div class="check-form d-flex mr-1">
                                                                <asp:CheckBox ID="type" CssClass="check-form-input check-model" runat="server" Visible="false"/>
                                                                <asp:Label ID="Label3" AssociatedControlID="type" runat="server">
                                                                    <img src="/image/default_assets/theholybook.jpg" alt="Holy Light"/>
                                                                    <span class="fa fa-check"></span>
                                                                </asp:Label>
                                                            </div>
                                                            <div class="my-auto">
                                                                <p class="name">
                                                                    <asp:Label ID="nameLabel" runat="server" Text='<%# Eval("name") %>' />
                                                                </p>
                                                                <p class="description">
                                                                    <asp:Label ID="descriptionLabel" runat="server" 
                                                                    Text='<%# Eval("description") %>' />    
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                    <EmptyDataTemplate>
                                                        <span>No data was returned.</span>
                                                    </EmptyDataTemplate>
                                                    <LayoutTemplate>
                                                        <div ID="itemPlaceholderContainer" runat="server" style="">
                                                            <span ID="itemPlaceholder" runat="server" />
                                                        </div>
                                                        <div style="">
                                                        </div>
                                                    </LayoutTemplate>
                                                </asp:ListView>
                                            </div>
                                            <div class="features-area pt-2">
                                                <div class="features">
                                                    <label><i class="fa fa-pencil" aria-hidden="true"></i></label>
                                                    <label><i class="fa fa-times" aria-hidden="true"></i></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
