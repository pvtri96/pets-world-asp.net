﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="PetCreation.aspx.cs" Inherits="PetsWorld.createapet" Title="Create a pet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="PetCreation" ContentPlaceHolderID="content" runat="server">
    <div class="pet-create model" >
        <div class="col-10 col-sm-8 col-md-7 col-lg-7 col-xl-6 create-body">
            <div class="create-list col-12 col-sm-6 col-md-6 col-lg-6 col-xl-7 ">
                <div class="create-title" >
                    <h3 class="title">Create a pet</h3>
                </div>
                <p class="layout-description">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur commodi eum quae, ipsum modi odio nulla, aliquam blanditiis dolorem.
                </p>
                <legend>
                    General
                </legend>
                <div class="form-group">
                    <asp:Label AssociatedControlID="name" Text="Name" runat="server"/>
                    <asp:TextBox CssClass="form-control form-control-success form-control-danger font-format" ID="name" runat="server"/>
                </div>
                <div class="form-group">
                    <asp:Label AssociatedControlID="desc" runat="server" Text="Description"/>
                    <asp:TextBox CssClass="form-control font-format" ID="desc" runat="server"/>
                </div>
                <legend>
                    Photo
                </legend>
                <div class="photo">
                    <div class="col-12 no-spacing photo-square">
                        <div class="form-group cover">
                            <asp:Label Text="Landscape Photo" runat="server"/>
                            <div></div>
                        </div>
                    </div>
                </div>
                <legend>
                    Information
                </legend>
                <div class="form-group">
                    <label class="form-control-label" for="rad-group">Gender</label>
                    <div class="radio-group" id="rad-group">
                        <div class="form-check">
                            <asp:Label CssClass="form-check-label" AssociatedControlID="radgroup" runat="server">
                                <asp:RadioButtonList CssClass="form-check-input" ID="radgroup" RepeatDirection="Horizontal" runat="server">
                                    <asp:ListItem Value="" Text="Male"></asp:ListItem>
                                    <asp:ListItem Value="" Text="Female"></asp:ListItem>
                                    <asp:ListItem Value="" Text="Other"></asp:ListItem>
                                </asp:RadioButtonList>
                            </asp:Label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label AssociatedControlID="dropdown" Text="Type" runat="server"/>
                    <asp:DropDownList ID="dropdown" CssClass="form-control option-input"  runat="server">
                        <asp:ListItem Value="" Text="--Select one--"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div>
                    <div class="form-group">
                        <asp:Label AssociatedControlID="height" Text="Height" runat="server"/>
                        <asp:TextBox class="form-control form-control-success form-control-danger font-format" id="height" runat="server"/>
                    </div>
                    <div class="form-group">
                        <asp:Label AssociatedControlID="weight" Text="Weight" runat="server"/>
                        <asp:TextBox class="form-control form-control-success form-control-danger font-format" id="weight" runat="server"/>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label Text="Status" runat="server"/>
                </div>
                <div class="form-group">
                    <asp:Label AssociatedControlID="price" runat="server" Text="Price"/>
                    <asp:TextBox min="0" CssClass="form-control form-control-success form-control-danger font-format" ID="price" runat="server"/>
                </div>
                <div class="create-btn pt-2">
                    <div class="form-group ml-auto">
                        <asp:Button CssClass="btn btn-primary ml-1" Text="Create" runat="server"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
