﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="StatusManagement.aspx.cs" Inherits="PetsWorld.StatusManagement" Title="Status Management" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="StatusManagement" ContentPlaceHolderID="content" runat="server">
    <div class="model" id="main">
        <div class="row">
            <div class="create-area col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5">
                <div class="row">
                    <div class="create-area-content col-9 col-sm-11 col-md-10 col-lg-8 col-xl-8
                                justify-content-center align-items-center
                                mx-auto my-auto">
                        <h5 class="title">Status Management</h5>
                        <p class="layout-description">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur commodi eum quae, ipsum modi odio nulla, aliquam blanditiis dolorem.
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur commodi eum quae, ipsum modi odio nulla, aliquam blanditiis dolorem.
                        </p>
                        <div class="form-group">
                            <asp:Label ID="Label1" CssClass="form-control-label" AssociatedControlID="name" runat="server" Text="Name"></asp:Label>
                            <asp:TextBox id="name" CssClass="form-control form-control-success form-control-danger" runat="server"/>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="Label2" cssclass="form-control-label" AssociatedControlID="description" Text="Description" runat="server"></asp:Label>
                            <asp:TextBox ID="description" CssClass="form-control form-control-success form-control-danger" runat="server"></asp:TextBox>
                        </div>
                        <div class="form-group features">
                            <asp:Button ID="Button1" CssClass="btn btn-secondary" Text="Cancel" runat="server"/>
                            <asp:Button ID="Button2" CssClass="btn btn-primary ml-2" Text="Update" runat="server"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="list-area col-12 col-sm-7 col-md-7 col-lg-7 col-xl-7">
                <div class="row">
                    <div class="list-area-content col-9 col-sm-10 col-md-10 col-lg-8 col-xl-8
                                justify-content-center align-items-center
                                mx-auto my-auto">
                        <div class="form-group filter-area d-flex">
                            <asp:TextBox CssClass="form-control search-input" runat="server"/>
                            <asp:DropDownList CssClass="form-control option-input ml-1" runat="server">
                                <asp:ListItem Value="" Text="Select one"/>
                            </asp:DropDownList>
                            <asp:Button CssClass="btn btn-secondary btn-sm ml-1 reset-filter" runat="server" Text="Search"/>
                        </div>
                        <div class="option-bar row">
                            <div class="check-form col-12">
                                <input type="checkbox" value="" class="check-all-input" id="checkall" runat="server" />
                                <asp:Label ID="Label3" AssociatedControlID="checkall" class="check-all-title col-5" Text="Select all" runat="server"/>
                                <div class="options col-7 d-flex justify-content-end" ">
                                    <asp:Label ID="Label4" CssClass="uncheck-option" runat="server"/>
                                    <div class="delete-option">
                                        <i class="fa fa-times"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list">
                            <div class="list-group">
                                <div class="list-group-item media">
                                    <div class="media-body">
                                        <div class="row">
                                            <div class="info col-12 text-truncate">
                                                <asp:ListView ID="ListView_PetStatus" runat="server" DataKeyNames="id">
                                                    <ItemTemplate>
                                                        <div class="col-12 d-flex py-2">
                                                            <div class="check-form d-flex mr-1">
                                                            <asp:CheckBox ID="CheckBox1" CssClass="check-form-input check-model" runat="server" Visible="false"/>
                                                            <asp:Label ID="status" AssociatedControlID="status" runat="server">
                                                                <img src="/image/default_assets/theholybook.jpg" alt="Holy Light"/>
                                                                <span class="fa fa-check"></span>
                                                            </asp:Label>
                                                        </div>
                                                        <div class="my-auto">                                                    <p class="name"><asp:Label ID="nameLabel" runat="server" Text='<%# Eval("name") %>' /></p>
                                                            <p class="description"><asp:Label ID="descriptionLabel" runat="server" 
                                                                Text='<%# Eval("description") %>' /></p>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                    <EmptyDataTemplate>
                                                        <span>No data was returned.</span>
                                                    </EmptyDataTemplate>
                                                    <LayoutTemplate>
                                                        <div ID="itemPlaceholderContainer" runat="server" style="">
                                                            <span ID="itemPlaceholder" runat="server" />
                                                        </div>
                                                        <div style="">
                                                        </div>
                                                    </LayoutTemplate>
                                                </asp:ListView>
                                            </div>
                                            <div class="features-area pt-2">
                                                <div class="features">
                                                    <label><i class="fa fa-pencil" aria-hidden="true"></i></label>
                                                    <label><i class="fa fa-times" aria-hidden="true"></i></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
