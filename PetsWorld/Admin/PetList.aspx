﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="PetList.aspx.cs" Inherits="PetsWorld.PetList" Title="Pet List" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .card-img-top 
        {
        	width: auto !important;
        	height: 400px !important;
        	object-fit: cover;
        }
    </style>
</asp:Content>
<asp:Content ID="PetList" ContentPlaceHolderID="content" runat="server">
    <div class="layout">
        <div class="frame">
            <div class="d-flex tabs mr-3">
                <div class="col-6">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" href="#">Dogs</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Cats</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Birds</a>
                        </li>
                    </ul>
                </div>
                <div class="col-6 input-group px-0">
                    <div class="d-flex ml-auto">
                        <input type="text" id="Search" class="pl-2" placeholder="Search for pets">
                    </div>
                </div>
            </div>
            <div class="content">
                <div class="card-group">
                    <asp:ListView ID="ListView1" runat="server" DataKeyNames="id" 
                        DataSourceID="PetsListDataSource">
                        <ItemTemplate>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                                <div class="card my-3">
                                    <div class="d-flex card-footer no-background no-border p-2">
                                        <span class="col no-spacing button ml-auto">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-secondary mr-2" data-toggle="modal" data-target="#myModal-<%# Eval("id") %>"><span class="fa fa-eye"></span></button>
                                                <button type="button" class="btn btn-primary mr-2"><span class="fa fa-pencil"></span></button>
                                                <button type="button" class="btn btn-danger"><span class="fa fa-trash"></span></button>
                                            </div>
                                        </span>
                                    </div>
                                    <asp:Image ID="Image4" runat="server" ImageUrl='<%# "/image/default_assets/" + Eval("photo_landscape") %>' CssClass="card-img-top"/>
                                    <div class="card-block px-2 py-2 font">
                                        <div class="d-flex">
                                            <h4 class="card-title mx-auto">
                                                <asp:Label ID="nameLabel" runat="server" Text='<%# Eval("name") %>' />
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="myModal-<%# Eval("id") %>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <div class="d-flex">
                                                <h4 class="card-title mx-auto my-auto">
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("name") %>' />
                                                </h4>
                                            </div>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body pt-2 pb-0 px-2">
                                            <div class="container-fluid py-0">
                                                <div class="card no-border">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "/image/default_assets/" + Eval("photo_landscape") %>' Width="100%"/>
                                                    <div class="card-block px-2 py-2 font">
                                                        <div class="d-flex flex-column my-2 font-14">
                                                            <span class="my-auto font-bold"><asp:Label ID="sexLabel" runat="server" Text='<%# ((int)Eval("sex") == 1) ? "Male" : "Female" %>' />
                                                            <span class="my-auto font-bold">Height: <asp:Label ID="heightLabel" runat="server" Text='<%# Eval("height") + " cm" %>' /></span>
                                                            <span class="my-auto font-bold">Weight: <asp:Label ID="weightLabel" runat="server" Text='<%# Eval("weight") + " kg" %>' /></span>
                                                            <span class="my-auto font-bold">Type: Golden Retriever</span>
                                                            <span class="font-bold">Status: Healthy, Strong, No Diseases.</span>
                                                            <span class="font-bold">Description: <asp:Label ID="descriptionLabel" runat="server" 
                                                                Text='<%# Eval("description") %>' /></span>
                                                            <span class="my-auto mr-auto font-bold">Price: <asp:Label ID="priceLabel" runat="server" Text='<%# Eval("price") + " VND" %>' /></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <span>No data was returned.</span>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <div ID="itemPlaceholderContainer" runat="server" style="" class="row">
                                <span ID="itemPlaceholder" runat="server" />
                            </div>
                            <div style="">
                            </div>
                        </LayoutTemplate>
                    </asp:ListView>
                    <asp:SqlDataSource ID="PetsListDataSource" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:PetsWorldDBConnection %>" 
                        SelectCommand="SELECT * FROM [pets] ORDER BY [id] DESC"></asp:SqlDataSource>
                </div>
            </div>
            <div class="">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1">Previous</a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">...</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#">Next</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</asp:Content>
