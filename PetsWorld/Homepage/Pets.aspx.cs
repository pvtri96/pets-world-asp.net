﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace PetsWorld.Homepage
{
    public partial class Pets : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getAllPetTypes();

            if (Request.QueryString["id"].ToString().Length != 0)
            {
                ListView_Pets.DataSourceID = "PetsByTypeDataSource";
            }
            else
            {
                ListView_Pets.DataSourceID = "AllPetsDataSource";
            }
            ListView_Pets.DataBind();
        }

        private void getAllPetTypes()
        {
            string query = string.Format(@"SELECT * FROM pet_types");

            DataTable dt = Handler.SelectModels(query);
            ListView_PetTypes_Homepage.DataSource = dt;
            ListView_PetTypes_Homepage.DataBind();
        }
    }
}
