﻿<%@ Page Language="C#" MasterPageFile="~/Homepage/Main.Master" AutoEventWireup="true" CodeBehind="Pets.aspx.cs" Inherits="PetsWorld.Homepage.Pets" Title="Pet's World" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
<div class="pet-ad">
    <div id="pet-ad-control" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
                <img class="d-block img-fluid" src="/image/default_assets/ad-1.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid" src="/image/default_assets/ad-2.jpg" alt="Second slide">
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid" src="/image/default_assets/ad-3.jpg" alt="Third slide">
            </div>
        </div>
        <a class="carousel-control-prev" data-target="#pet-ad-control" role="button" data-slide="prev">
           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
           <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" data-target="#pet-ad-control" role="button" data-slide="next">
           <span class="carousel-control-next-icon" aria-hidden="true"></span>
           <span class="sr-only">Next</span>
        </a>
        <ol class="carousel-indicators">
            <li data-target="#pet-ad-control" data-slide-to="0" class="active"></li>
            <li data-target="#pet-ad-control" data-slide-to="1"></li>
            <li data-target="#pet-ad-control" data-slide-to="2"></li>
        </ol>
    </div>
</div>
<div class="pet-body container mb-5">
    <div class="row">
        <div class="pet-sidebar col-12 col-md-3">
            <div class="sort">
                <div class="pet">
                    <button class="pet-btn" type="button" data-toggle="collapse" data-target="#collapsePet" aria-expanded="false" aria-controls="collapsePet">
                        <b>Pets</b>
                    </button>
                    <div class="collapse" id="collapsePet">
                        <div class="card">
                            <div class="form-check">
                                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='Pets.aspx?id='>
                                    All
                                </asp:HyperLink>
                            </div>
                        </div>
                        <asp:ListView ID="ListView_PetTypes_Homepage" runat="server" DataKeyNames="id">
                            <ItemTemplate>
                                <div class="card">
                                    <div class="form-check">
                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "Pets.aspx?id=" + Eval("id") %>'>
                                            <%# Eval("name") %>
                                        </asp:HyperLink>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                                <span>No data was returned.</span>
                            </EmptyDataTemplate>
                            <LayoutTemplate>
                                <div ID="itemPlaceholderContainer" runat="server" style="">
                                    <span ID="itemPlaceholder" runat="server" />
                                </div>
                                <div style="">
                                </div>
                            </LayoutTemplate>
                        </asp:ListView>
                    </div>
                    <hr>
                </div>
                <div class="price">
                    <button class="price-btn" type="button" data-toggle="collapse" data-target="#collapsePrice" aria-expanded="false" aria-controls="collapsePrice">
                        <b>Price</b>
                    </button>
                    <div class="collapse" id="collapsePrice">
                        <div class="card">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option1"> <200.000đ
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2"> 200.000đ - 500.000đ
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option3"> 500.000đ - 1.000.000đ
                                </label>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
        <div class="main-item col-sm-12 col-md-9">
            <div class="page-number-and-sort col-12 row justify-content-between">
                <select class="custom-select mb-2  mb-sm-0" id="inlineFormCustomSelect">
                    <option selected>Sort by...</option>
                    <option value="1">Price: high to low</option>
                    <option value="2">Price: low to high</option>
                    <option value="3">Best seller</option>
                    <option value="4">Top rates</option>
                    <option value="5">Species</option>
                </select>
                <div
                    dir-pagination-controls
                    boudary-link="true"
                    pagination-id="petsPagination"
                    template-url="/views/load?path=directives.dir-pagination" class="hidden-sm-down">
                </div>
            </div>
            <div class="item">
                <div class="card-group col-md-12">
                    <asp:ListView ID="ListView_Pets" runat="server" DataKeyNames="id">
                        <ItemTemplate>
                            <div class="col-12 col-sm-6 col-md-3 item-detail">
                                
                                <div class="card-item card">
                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "PetDetails.aspx?id=" + Eval("id") %>'>
                                        <div class="avatar">
                                            <asp:Image ID="photoLabel" runat="server" ImageUrl='<%# "/image/default_assets/" + Eval("photo_landscape") %>' CssClass="card-img-top"/>
                                        </div>
                                    </asp:HyperLink>
                                    <div class="card-block">
                                        <div class="describe">
                                            <p6><asp:Label ID="nameLabel" runat="server" Text='<%# Eval("name") %>' /></p6>
                                        </div>
                                        <div class="price">
                                            <h5><asp:Label ID="Label1" runat="server" Text='<%# Eval("price") %>' /></h5>
                                            <div class="sale-off">
                                                <p3><del>600.000 VND</del> -20%</p3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <span>No data was returned.</span>
                        </EmptyDataTemplate>
                        <LayoutTemplate>
                            <div ID="itemPlaceholderContainer" runat="server" style="" class="row">
                                <span ID="itemPlaceholder" runat="server" />
                            </div>
                            <div style="">
                            </div>
                        </LayoutTemplate>
                    </asp:ListView>
                    <asp:SqlDataSource ID="PetsByTypeDataSource" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:PetsWorldDBConnection %>" 
                        SelectCommand="SELECT [name], [id], [type_id], [price], [photo_landscape] FROM [pets] WHERE ([type_id] = @id) ORDER BY id DESC">
                        <SelectParameters>
                            <asp:QueryStringParameter DefaultValue="1" Name="id" 
                                QueryStringField="id" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="AllPetsDataSource" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:PetsWorldDBConnection %>" 
                        SelectCommand="SELECT [name], [id], [type_id], [price], [photo_landscape] FROM [pets] ORDER BY id DESC">
                        <SelectParameters>
                            <asp:QueryStringParameter DefaultValue="1" Name="id" 
                                QueryStringField="id" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </div>
        </div>
    </div>
    <div class="show-more-btn hidden-sm-up row justify-content-center">
        <button type="button" name="show-more" >Show more</button>
    </div>
</div>
    </form>
</asp:Content>
