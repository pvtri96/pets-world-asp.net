﻿<%@ Page Language="C#" MasterPageFile="~/Homepage/Main.Master" AutoEventWireup="true" CodeBehind="PetDetails.aspx.cs" Inherits="PetsWorld.Homepage.Pet_Details" Title="Pet's World" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
    <div class="body">
    <div class="container">
        <div class="link">
            <ol class="breadcrumb bg-white">
                <li class="breadcrumb-item"><asp:HyperLink ID="Homepage" NavigateUrl="Index.aspx" runat=server>Home</asp:HyperLink></li>
                <li class="breadcrumb-item"><asp:HyperLink ID="Pets" NavigateUrl="Pets.aspx?id=" runat=server>Pets</asp:HyperLink></li>
                <li class="breadcrumb-item active"><span>Pet Details</span></li>
            </ol>
        </div>
        
        <asp:ListView ID="ListView1" runat="server" DataKeyNames="id" 
            DataSourceID="PetDetailDataSource">
            <ItemTemplate>
                <div class="row d-flex">
                    <div class="img col-md-5">
                        <div class="img-card col-md-12">
                            <asp:Image ID="photoLabel" runat="server" ImageUrl='<%# "/image/default_assets/" + Eval("photo_landscape") %>' CssClass="img-responsive"/>
                        </div>
                        <div class="card-group col-md-12">
                            <div id="img-control" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner" role="listbox">
                                   <div class="carousel-item active">
                                        <div class="col-12 col-sm-6 col-md-3 img-mini">
                                            <div class="card-item card">
                                                <div class="avatar">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl='<%# "/image/default_assets/" + Eval("photo_landscape") %>' CssClass="card-img-top"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 img-mini">
                                            <div class="card-item card">
                                                <div class="avatar">
                                                    <asp:Image ID="Image4" runat="server" ImageUrl='<%# "/image/default_assets/" + Eval("photo_landscape") %>' CssClass="card-img-top"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 img-mini">
                                            <div class="card-item card">
                                                <div class="avatar">
                                                    <asp:Image ID="Image5" runat="server" ImageUrl='<%# "/image/default_assets/" + Eval("photo_landscape") %>' CssClass="card-img-top"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 img-mini">
                                            <div class="card-item card">
                                                <div class="avatar">
                                                    <asp:Image ID="Image6" runat="server" ImageUrl='<%# "/image/default_assets/" + Eval("photo_landscape") %>' CssClass="card-img-top"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="col-12 col-sm-6 col-md-3 img-mini">
                                            <div class="card-item card">
                                                <div class="avatar">
                                                    <asp:Image ID="Image7" runat="server" ImageUrl='<%# "/image/default_assets/" + Eval("photo_landscape") %>' CssClass="card-img-top"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 img-mini">
                                            <div class="card-item card">
                                                <div class="avatar">
                                                    <asp:Image ID="Image8" runat="server" ImageUrl='<%# "/image/default_assets/" + Eval("photo_landscape") %>' CssClass="card-img-top"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" data-target="#img-control" role="button" data-slide="prev">
                               <i class="fa fa-chevron-left" aria-hidden="true"></i>
                               <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" data-target="#img-control" role="button" data-slide="next">
                              <i class="fa fa-chevron-right" aria-hidden="true"></i>
                               <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="detail col-md-7">
                        <div class="title">
                            <h1><asp:Label ID="nameLabel" runat="server" Text='<%# Eval("name") %>' /></h1>
                        </div>
                        <div class="describe">
                            <h5>Description: </h5>
                            <p><asp:Label ID="descriptionLabel" runat="server" 
                                Text='<%# Eval("description") %>' /></p>
                        </div>
                        <div class="gender">
                            <h5>Gender: <span><asp:Label ID="sexLabel" runat="server" Text='<%# ((int)Eval("sex") == 1) ? "Male" : "Female" %>' /></span></h5>
                        </div>
                        <div class="weight">
                            <h5>Weight: <span><asp:Label ID="weightLabel" runat="server" Text='<%# Eval("weight") + " kg" %>' /></span></h5>
                        </div>
                        <div class="height">
                            <h5>Height: <span><asp:Label ID="heightLabel" runat="server" Text='<%# Eval("height") + " cm" %>' /></span></h5>
                        </div>
                        <hr>
                        <div class="price">
                            <h3>Price: 400.000 VND</h3>
                            <div class="sale-off">
                                <h6>Before Sale-off: <asp:Label ID="Label1" runat="server" Text='<%# Eval("price") %>' /> VND</h6>
                                <h6>Save: 20%</h6>
                            </div>
                        </div>
                        <div class="shop d-flex">
                            <h6>Shop:</h6>
                            <a href="{{ route('view.home.index') }}">Pet's House</a>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="row2 d-flex">
                    <div class=" img col-md-9">
                        <asp:Image ID="Image2" runat="server" ImageUrl='<%# "/image/default_assets/" + Eval("photo_landscape") %>'/>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        <asp:Image ID="Image3" runat="server" ImageUrl='<%# "/image/default_assets/" + Eval("photo_landscape") %>'/>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                    </div>
                    <div class="card related-accessories col-md-3">
                        <h4>Related Accessories</h4>
                        <div class="item">
                            <div class="card-item card">
                                <div class="img">
                                    <img class="card-img-top" src="/image/default_assets/new-product-1.jpg" alt="Card image cap">
                                </div>
                                <div class="card-block">
                                    <div class="describe">
                                        <p6>Name-Size-Color-Origin</p6>
                                    </div>
                                    <div class="price">
                                        <h5>480.000đ VND</h5>
                                        <div class="sale-off">
                                            <p3><del>600.000 VND</del> -20%</p3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-item card">
                                <div class="img">
                                    <img class="card-img-top" src="/image/default_assets/new-product-1.jpg" alt="Card image cap">
                                </div>
                                <div class="card-block">
                                    <div class="describe">
                                        <p6>Name-Size-Color-Origin</p6>
                                    </div>
                                    <div class="price">
                                        <h5>480.000đ VND</h5>
                                        <div class="sale-off">
                                            <p3><del>600.000 VND</del> -20%</p3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-item card">
                                <div class="img">
                                    <img class="card-img-top" src="/image/default_assets/new-product-1.jpg" alt="Card image cap">
                                </div>
                                <div class="card-block">
                                    <div class="describe">
                                        <p6>Name-Size-Color-Origin</p6>
                                    </div>
                                    <div class="price">
                                        <h5>480.000đ VND</h5>
                                        <div class="sale-off">
                                            <p3><del>600.000 VND</del> -20%</p3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-item card">
                                <div class="img">
                                    <img class="card-img-top" src="/image/default_assets/new-product-1.jpg" alt="Card image cap">
                                </div>
                                <div class="card-block">
                                    <div class="describe">
                                        <p6>Name-Size-Color-Origin</p6>
                                    </div>
                                    <div class="price">
                                        <h5>480.000đ VND</h5>
                                        <div class="sale-off">
                                            <p3><del>600.000 VND</del> -20%</p3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-item card">
                                <div class="img">
                                    <img class="card-img-top" src="/image/default_assets/new-product-1.jpg" alt="Card image cap">
                                </div>
                                <div class="card-block">
                                    <div class="describe">
                                        <p6>Name-Size-Color-Origin</p6>
                                    </div>
                                    <div class="price">
                                        <h5>480.000đ VND</h5>
                                        <div class="sale-off">
                                            <p3><del>600.000 VND</del> -20%</p3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-item card">
                                <div class="img">
                                    <img class="card-img-top" src="/image/default_assets/new-product-1.jpg" alt="Card image cap">
                                </div>
                                <div class="card-block">
                                    <div class="describe">
                                        <p6>Name-Size-Color-Origin</p6>
                                    </div>
                                    <div class="price">
                                        <h5>480.000đ VND</h5>
                                        <div class="sale-off">
                                            <p3><del>600.000 VND</del> -20%</p3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
            <EmptyDataTemplate>
                <span>No data was returned.</span>
            </EmptyDataTemplate>
            <LayoutTemplate>
                <div ID="itemPlaceholderContainer" runat="server" style="">
                    <span ID="itemPlaceholder" runat="server" />
                </div>
                <div style="">
                </div>
            </LayoutTemplate>
        </asp:ListView>
        <asp:SqlDataSource ID="PetDetailDataSource" runat="server" 
            ConnectionString="<%$ ConnectionStrings:PetsWorldDBConnection %>" 
            SelectCommand="SELECT * FROM [pets] WHERE ([id] = @id)">
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="1" Name="id" 
                    QueryStringField="id" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <div class="comment row d-flex">
            <div class="form-group col-9">
                <label for="cmt"><h5>Comments</h5></label>
                <textarea class="form-control" id="cmt" rows="3" placeholder="Enter your comments ..."></textarea>
                <div class="btn">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
    </form>
</asp:Content>
