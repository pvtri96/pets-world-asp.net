﻿<%@ Page Language="C#" MasterPageFile="~/Homepage/Main.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="PetsWorld.Homepage.Index" Title="Pet's World" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form id="form1" runat="server">
    <div class="main-background">
        <div class="slogan">
            <h1>The Dog</h1>
            <h3>is the only thing on earth <br>that will love you more than <br>you love yourself.</h3>
        </div>
    </div>
    <div class="body">
    <div class="list container">
        <div class="tab best-seller hidden-sm-down">
            <div class="title">
                <p3>Top sellers of the month</p3>
            </div>
            
            <div class="item col-10 col-md-12 row even">
                <div class="img promotion-ad best-seller col-md-3 ">
                    <div class="title-img row justify-content-center ">
                        <h5>Top seller of the month</h5>
                    </div>
                    <div class="button buy-now row justify-content-center">
                        <asp:HyperLink ID="BuyNow1" runat=server><button type="button" name="button">Buy Now</button></asp:HyperLink>
                    </div>
                </div>
                <div class="card-group col-md-9">
                    <div id="hot-item-control" class="carousel slide">
                        <asp:ListView ID="ListView_Pets" runat="server" DataKeyNames="id" >
                            <ItemTemplate>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "PetDetails.aspx?id=" + Eval("id") %>'>
                                            <div class="avatar">
                                                <asp:Image ID="photoLabel" runat="server" ImageUrl='<%# "/image/default_assets/" + Eval("photo_landscape") %>' CssClass="card-img-top"/>
                                            </div>
                                        </asp:HyperLink>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6><asp:Label ID="nameLabel" runat="server" Text='<%# Eval("name") %>' /></p6>
                                            </div>
                                            <div class="price">
                                                <h5><asp:Label ID="priceLabel" runat="server" Text='<%# Eval("price") %>' /></h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                                <span>No data was returned.</span>
                            </EmptyDataTemplate>
                            <LayoutTemplate>
                                <div ID="itemPlaceholderContainer" runat="server" style="" class="row">
                                    <span ID="itemPlaceholder" runat="server" />
                                </div>
                                <div style="">
                                </div>
                            </LayoutTemplate>
                        </asp:ListView>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab sale-off hidden-sm-down">
            <div class="title">
                <p3>Sale off - <i>Summer Promotion</i></p3>
            </div>
            <div class="item col-10 col-md-12 row odd">
                <div class="img promotion-ad sale-off col-md-3">
                    <div class="title-img row justify-content-center ">
                        <h5>SALE OFF-<i>Summer Promotion</i></h5>
                    </div>
                    <div class="button buy-now row justify-content-center">
                        <asp:HyperLink ID="BuyNowLink2" runat=server><button type="button" name="button">Buy Now</button></asp:HyperLink>
                    </div>
                </div>
                <div class="card-group col-md-9">
                    <div id="sale-off-control" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="lisstbox">
                            <div class="carousel-item active">
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-1.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-1.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-1.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-1.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-2.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-2.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-2.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-2.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-3.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-3.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-3.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-3.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" data-target="#sale-off-control" role="button" data-slide="prev">
                       <i class="fa fa-chevron-left" aria-hidden="true"></i>
                       <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" data-target="#sale-off-control" role="button" data-slide="next">
                      <i class="fa fa-chevron-right" aria-hidden="true"></i>
                       <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="tab new-item  hidden-sm-down">
            <div class="title">
                <p3>New Products - <i>Best for your pet</i></p3>
            </div>
            <div class="item pet col-10 col-md-12 row even">
                <div class="img promotion-ad new-item col-md-3">
                    <div class="title-img row    justify-content-center">
                        <h5>NEW PRODUCT  <br><i>  Best for your pet</i></h5>
                    </div>
                    <div class="button buy-now row justify-content-center">
                        <asp:HyperLink ID="BuyNowLink3" runat=server><button type="button" name="button">Buy Now</button></asp:HyperLink>
                    </div>
                </div>
                <div class="card-group col-md-9">
                    <div id="new-product-control" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="lisstbox">
                            <div class="carousel-item active">
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-1.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-1.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-1.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-1.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-2.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-2.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-2.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-2.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-3.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-3.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-3.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-md-3 item-detail">
                                    <div class="card-item card">
                                        <div class="avatar">
                                            <img class="card-img-top" src="/image/default_assets/dog-accessories-3.jpg" alt="Card image cap">
                                        </div>
                                        <div class="card-block">
                                            <div class="describe">
                                                <p6>Name-Size-Color-Origin</p6>
                                            </div>
                                            <div class="price">
                                                <h5>480.000đ VND</h5>
                                                <div class="sale-off">
                                                    <p3><del>600.000 VND</del> -20%</p3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <a class="carousel-control-prev" data-target="#new-product-control" role="button" data-slide="prev">
                       <i class="fa fa-chevron-left" aria-hidden="true"></i>
                       <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" data-target="#new-product-control" role="button" data-slide="next">
                      <i class="fa fa-chevron-right" aria-hidden="true"></i>
                       <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div id="control-ad" class="carousel slide hidden-sm-up" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                    <div class="img promotion-ad best-seller col-md-3 ">
                        <div class="title-img row justify-content-center ">
                            <h5>Top seller of the month</h5>
                        </div>
                        <div class="button buy-now row justify-content-center ">
                            <a href="http://localhost:3000/pets"><button type="button" name="button">Buy Now</button></a>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="img promotion-ad sale-off col-md-3">
                        <div class="title-img row justify-content-center ">
                            <h5>SALE OFF-<i>Summer Promotion</i></h5>
                        </div>
                        <div class="button buy-now row justify-content-center">
                            <a href="http://localhost:3000/pets"><button type="button" name="button">Buy Now</button></a>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="img promotion-ad new-item col-md-3">
                        <div class="title-img row    justify-content-center">
                            <h5>NEW PRODUCT  <br><i>  Best for your pet</i></h5>
                        </div>
                        <div class="button buy-now row justify-content-center">
                            <a href="http://localhost:3000/pets"><button type="button" name="button">Buy Now</button></a>
                        </div>
                    </div>
                    
                </div>
            </div>
            <a class="carousel-control-prev" data-target="#control-ad" role="button" data-slide="prev">
               <span class="carousel-control-prev-icon" aria-hidden="true"></span>
               <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" data-target="#control-ad" role="button" data-slide="next">
               <span class="carousel-control-next-icon" aria-hidden="true"></span>
               <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>
    </form>
</asp:Content>
