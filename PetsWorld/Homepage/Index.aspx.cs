﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

namespace PetsWorld.Homepage
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            getAllPets();
        }

        private void getAllPets()
        {
            string query = string.Format(@"SELECT TOP 4 * FROM pets ORDER BY id DESC");

            DataTable dt = Handler.SelectModels(query);
            ListView_Pets.DataSource = dt;
            ListView_Pets.DataBind();
        }
    }
}
